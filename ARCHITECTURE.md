# System architecture

## Data Models

### Users

| Column | Datatype |
| --- | --- |
| id | int/string |
| name | string |
| email | string  |
| mobile | string |
| type | ENUM |
| password | string |

// Type - worker, health officer


### Mothers

| Column | Datatype |
| --- | --- |
| id | int/string |
| name | string |
| type | ENUM |
| age | int  |
| blood_group | string  |
| mobile | string |
| street_name | string |
| area | string |
| city | string |
| pin_code | string |
| delivery_date | date time (yyyy-mm-dd HH:mm:ss:a) |
| child_weight | string |
| child_gender | string |
| current_visit | int |


// Type : mother


### Content

| Column | Datatype |
| --- | --- |
| Title | string | 
| URL | string | 
| Stage | ENUM |
| ID | int |

### Meeting

| Column | Datatype |
| --- | --- |
| Mother | foreignkey |
| DateTime | datetime |
| HealthWorker | foreign_key |
| Trax | file |
| ID | int |

### PHC

| Column | Datatype |
| --- | --- |
| Name | |
| Address | |
| Mothers | |
| Healthworkers | |

## Relationships

### Health Officer - Primary Health Center
- A HO belongs to a PHC
- A PHC can have only one HO

### Health Worker - Primary Health Center
- A HW belongs to a PHC
- A PHC can have many HW

### Health Officer - Health Worker
- A HO can have many HW under them
- A HW can belong to only one HO

### Health Worker - Mother
- A HW will be visiting many Mothers
- A Mother will be visited by a HW

## REST API

### USER

CRUD

### MEETING 

CRUD 

### 

CRUD 

## Android - Example Couchbase data format


**User**

```
{
    "name" : "worker1",
    "password" : "texting",
    "admin_channels" : [ "worker1", "lawspet"],
    "admin_roles" : ["worker"]
}
```

**Mother**

```
{
    "_id":"3701700a-a225-454a-8f11-ecf01e9e8328",
    "_rev":"1-d99ad5023aa02bc1662c264b7920d86aae8e9423",
    "age":23,
    "area":"Thurukam",
    "blood_group":"A-",
    "channels":["lawspet","worker1"],
    "child_gender":"Female",
    "child_weight":2.6,
    "city":"Pondy",
    "create_at":"2019-05-14T14:23:13.475Z",
    "current_visit":1,
    "delivery_date":"2019-05-07 07:51:59",
    "dr_no":"17",
    "name":"Kee",
    "patient_id":"",
    "phone":"9604455464",
    "pin_code":"608301",
    "street_name":"Pukkulam",
    "type":"mother",
    "update_at":"2019-05-14T14:23:13.474Z",
    "visits":
        {
            "1":
                {
                    "questions":
                        {
                            "Adherence to Iron and Folic Acid supplementation":false,
                            "Care of the newborn":false,
                            "Exclusive breastfeeding":false,
                            "Nutritional advice":false,
                            "Personal hygiene":false
                        },
                    "schedule_date":"2019-05-13"
                },
           "2":
                {
                    "questions":
                        {
                            "Exclusive breastfeeding":false,
                            "Nutritional advice for the mothers":false
                        },
                    "schedule_date":"2019-05-20"
                },
            "3":
                {
                    "questions":
                        {
                            "Care of the newborn":false,
                            "Exclusive breastfeeding":false,
                            "Immunization of the baby":false,
                            "Nutritional advice for the mothers":false
                        },
                    "schedule_date":"2019-06-03"
                }
        }
}
```


