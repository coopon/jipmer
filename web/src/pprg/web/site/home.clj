(ns pprg.web.site.home
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [pprg.web.site :as site]
            [pprg.web.site.auth :as site.auth]
            [buddy.auth :as buddy.auth]))

(defn home-page [request]
  ;; (if (buddy.auth/authenticated? request)
  (ring-resp/response (site/base "JIPMER"
                                 [:div.column.is-half.is-offset-one-quarter
                                  [:h1.title.has-text-centered "JIPMER"]
                                  [:h2.has-size-3 "Health workers list"]]))

  ;;   (buddy.auth/throw-unauthorized)))
        ;;(assoc :session {:sexy "cool"} 
              ;;:status 401                         
          ;;     :cookies {:bird "kek"}) 
  )
