(ns pprg.web.site.mother
  (:require [hiccup.core :as hiccup]
            [ring.util.response :as ring-resp]
            [pprg.web.site :as site]
            [pprg.web.site.auth :as site.auth]
            [buddy.auth :as buddy.auth]
            [clj-http.client :as client]))

(defn create-mother-page [request]
  (ring-resp/response (site/base "Create Health Worker"
                                 [:div.column.is-half.is-offset-one-quarter
                                  [:h1.title.has-text-centered "Add Mother record"]
;;                                 [:h2.has-size-3 "Hello"]
;;                                 [:h2.has-size-3 "Kek"]
                                  [:form {:method "POST" :action "/mother/post"}
                                   (site/form-control "MCT Card No.")
                                   (site/form-control "Name")
                                   (site/form-control "Age")
                                   (site/form-control "DOB")
                                   (site/form-control "Expected date of delivery")
                                   (site/form-control "Address")
                                   (site/form-control "Baby Sex")
                                   (site/form-control "Baby Weight")
                                   (site/form-control-dropdown "Center" ["Kurichikuppam" "Lawspet"])
                                   (site/form-control-button "Submit")]])))

(defn worker-profile-control [request]
  [])

(defn create-user-couch
  "Creates a Sync gateway user"
  [request]
  (let [params (:params request)]
    (client/post "http://localhost:4985/jipmer/"
                 {:form-params
                  {:name (:name params)  ,
                   :password (:password params),
                   :admin_channels  [(:username params)
                                     "Kurichikuppam"],
                   :admin_roles ["nurse"],
                   :email (:email params),
                   :disabled true}

                  :content-type :json})))


  ;; {
 ;;  "name": "string",
;;   "password": "string",
;;   "admin_channels": [
  ;;     {{username}}
  ;;      {{health centre}}
;;   ],
;;   "admin_roles": [
;;     {{worker}}
;;   ],
;;   "email": "string",
;;   "disabled": true
  ;; }


(defn create-user-seed-data
  "Seed initial data for the syncgateway user"
  [request])

(defn create-mother-post
  "Accepts new user form POST data and processes the request"
  [request]
  (ring-resp/response (str (:params request))))
