-- :name users-create :! :n
-- :doc Insert a single user returning affected row count
INSERT INTO users (email,password)
VALUES (:email, :password)

-- :name users-by-email :? :n
-- :doc Get user by email
SELECT * FROM users
WHERE email = :email LIMIT 1

-- :name users-create-test :? :n
-- :doc Insert a single user returning affected row count
INSERT INTO users (email,password)
VALUES ("sskamalavelan", "hola")
