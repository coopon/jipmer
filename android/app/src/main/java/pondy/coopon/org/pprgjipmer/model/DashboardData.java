package pondy.coopon.org.pprgjipmer.model;

import java.util.HashMap;

public class DashboardData {

    private int noOfSchedule = 0, noOfVisited = 0, noOfPatients = 0;
    private String area;
    private HashMap<Integer, Integer> scheduleList = new HashMap<>();
    private HashMap<Integer, Integer> visitedList = new HashMap<>();

    public DashboardData() {
    }

    public int getNoOfSchedule() {
        return noOfSchedule;
    }

    public void setNoOfSchedule(int noOfSchedule) {
        this.noOfSchedule = noOfSchedule;
    }

    public int getNoOfVisited() {
        return noOfVisited;
    }

    public void setNoOfVisited(int noOfVisited) {
        this.noOfVisited = noOfVisited;
    }

    public int getNoOfPatients() {
        return noOfPatients;
    }

    public void setNoOfPatients(int noOfPatients) {
        this.noOfPatients = noOfPatients;
    }

    public HashMap<Integer, Integer> getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(HashMap<Integer, Integer> scheduleList) {
        this.scheduleList = scheduleList;
    }

    public HashMap<Integer, Integer> getVisitedList() {
        return visitedList;
    }

    public void setVisitedList(HashMap<Integer, Integer> visitedList) {
        this.visitedList = visitedList;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
