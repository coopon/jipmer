package pondy.coopon.org.pprgjipmer.helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;

import java.util.Set;

import pondy.coopon.org.pprgjipmer.activity.LoginActivity;
import pondy.coopon.org.pprgjipmer.model.User;

public class SharedPref {

    private static final String TAG = "SharedPref";

    // Shared Preferences
    private SharedPreferences pref;
     
    // Editor for Shared preferences
    private Editor editor;
     
    // Context
    private Context mContext;

    // Shared pref file name
    private static final String PREF_NAME = "pprg";

    /*
     * All Shared Preferences Keys
     */
    private static final String IS_LOGIN = "is_logged_in";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_CENTER_ID = "center_id";
    private static final String KEY_CENTER_NAME = "center_name";
    private static final String KEY_OLD_USER = "old_user";
    private static final String KEY_IS_NEW_LOGIN = "new_login";
    private static final String KEY_IS_MOTHER_LOGIN = "mother_login";

    // Constructor
    @SuppressLint("CommitPrefEdits")
    public SharedPref(Context context){
        this.mContext = context;
        // Shared pref mode
        int PRIVATE_MODE = 0;
        pref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
     
    /**
     * Create login session
     * */
    public void createLoginSession(User user){

        // mark user as logged in
        editor.putBoolean(IS_LOGIN, true);
         
        // Storing values in pref
        editor.putString(KEY_NAME, user.getName());
        editor.putString(KEY_EMAIL, user.getEmail());
        editor.putInt(KEY_CENTER_ID, user.getCenterId());
        editor.putString(KEY_CENTER_NAME, user.getCenterName());

        String workerName = user.getName();

        // check if it is not the same user
        if (pref.contains(KEY_OLD_USER) && pref.getString(KEY_OLD_USER, null) != null && !workerName.equals(pref.getString(KEY_OLD_USER, null)))
        {
            Log.d(TAG, "createLoginSession: " + workerName);
            DatabaseManager dbMgr = DatabaseManager.getSharedInstance(mContext);

            try {
                dbMgr.database.delete();
                DatabaseManager.clearInstance();

                Log.d (TAG, "Deleted local db");
                editor.remove(KEY_OLD_USER);
            } catch (CouchbaseLiteException e) {
                Log.e(TAG, "VAL LISTENER ERROR ON REMOVE");
                e.printStackTrace();
            }
        }

        // New login
        editor.putBoolean(KEY_IS_NEW_LOGIN, true);
        // commit changes
        editor.commit();
    }

    public void setNewLogin(Boolean isNewLogin)
    {
        editor.putBoolean(KEY_IS_NEW_LOGIN, isNewLogin);
        editor.commit();
    }

    public boolean isNewLogin() {
        return pref.getBoolean(KEY_IS_NEW_LOGIN, false);
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(Activity activity) {
        // Check login status
        if (!this.isLoggedIn()) {
            // Finish all activity
            activity.finishAffinity();

            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(mContext, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            mContext.startActivity(i);
        }
    }


    /**
     * Get stored session data
     * */
    public String getName()
    {
        return pref.getString(KEY_NAME, null);
    }
     
    /**
     * Clear session details
     * */
    public void logoutUser(){

        Log.d(TAG, "logoutUser: called");

        String oldUser = getName();

        Log.d(TAG, "logoutUser: oldUser" + oldUser);

        // Clearing all data from Shared Preferences
        editor.clear();
        if(!isMotherLoggedIn())
            editor.putString(KEY_OLD_USER, oldUser); // Existing user name
        editor.commit();
         
        // After logout redirect user to Login Activity
        Intent i = new Intent(mContext, LoginActivity.class);
        // Closing all the Activities & Add new Flag to start new Activity
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // Staring Login Activity
        mContext.startActivity(i);
    }
     
    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }


    public boolean isMotherLoggedIn(){
        return pref.getBoolean(KEY_IS_MOTHER_LOGIN, false);
    }

    public int getCenterId() {
        return pref.getInt(KEY_CENTER_ID, -1);
    }

    public String getCenterName() {
        return pref.getString(KEY_CENTER_NAME, "");
    }

    public void setMotherLogin() {
        // mark user as logged in
        editor.putBoolean(IS_LOGIN, true);

        // New login
        editor.putBoolean(KEY_IS_MOTHER_LOGIN, true);
        // commit changes
        editor.commit();
    }
}