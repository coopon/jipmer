package pondy.coopon.org.pprgjipmer.utils;

import android.content.Context;
import android.location.Location;

import org.oscim.backend.canvas.Bitmap;
import org.oscim.core.GeoPoint;
import org.oscim.layers.marker.MarkerItem;
import org.oscim.layers.marker.MarkerSymbol;

import static org.oscim.android.canvas.AndroidGraphics.drawableToBitmap;

public class MapUtils {

    public static MarkerItem createMarker(Context context, Location location, String title, String description, int resourceID) {
        GeoPoint point = new GeoPoint(location.getLatitude(), location.getLongitude());
        Bitmap bitmap_marker = drawableToBitmap(context.getResources().getDrawable(resourceID));
        MarkerSymbol markerSymbol = new MarkerSymbol(bitmap_marker, MarkerSymbol.HotspotPlace.BOTTOM_CENTER);
        MarkerItem markerItem = new MarkerItem(title, description, point);
        markerItem.setMarker(markerSymbol);
        return markerItem;
    }
}


