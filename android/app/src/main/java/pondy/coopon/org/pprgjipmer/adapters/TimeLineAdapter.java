package pondy.coopon.org.pprgjipmer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.vipulasri.timelineview.TimelineView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.model.TimeLineModel;
import pondy.coopon.org.pprgjipmer.utils.DateTimeUtils;
import pondy.coopon.org.pprgjipmer.utils.constant.VisitStatus;

public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineAdapter.ViewHolder> {

    private List<TimeLineModel> modelList;
    private Context mContext;

    public TimeLineAdapter(Context context, List<TimeLineModel> lineModels) {
        this.mContext = context;
        this.modelList = lineModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_visit_timeline, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        TimeLineModel master = modelList.get(position);
        switch (master.getStatus()) {
            case VisitStatus.INACTIVE:
                setMarker(holder, R.drawable.ic_timeline_inactive, R.color.timeline_inactive);
                break;
            case VisitStatus.ACTIVE:
                setMarker(holder, R.drawable.ic_timeline_active, R.color.timeline_active);
                break;
            case VisitStatus.COMPLETED:
                setMarker(holder, R.drawable.ic_timeline_completed, R.color.timeline_completed);
                break;
        }

        holder.txtScheduleDate.setText(master.getScheduleDate() != null ? DateTimeUtils.convertStringDateFormatToAnother(master.getScheduleDate(), DateTimeUtils.dateFormat_yyyy_MM_dd, DateTimeUtils.dateFormat_dd_MMM_yyyy) : "xx-xxx-xxxx");
        holder.txtVisitedDate.setText(master.getVisitedDate() != null ? DateTimeUtils.convertStringDateFormatToAnother(master.getVisitedDate(), DateTimeUtils.dateFormat_yyyy_MM_dd, DateTimeUtils.dateFormat_dd_MMM_yyyy) : "xx-xxx-xxxx");
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        holder.recyclerView.setAdapter(new TimeLineQuestionsAdapter(mContext, master.getVisitQuestions()));
        holder.txtVisitNo.setText(String.valueOf(master.getVisitNo()));

    }

    private void setMarker(ViewHolder holder, int drawableResId, int drawColor) {
        holder.timelineView.setMarker(ContextCompat.getDrawable(mContext, drawableResId), ContextCompat.getColor(mContext, drawColor));
    }


    @Override
    public int getItemCount() {
        return modelList.size();
    }

    /* adapter view holder */
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_schedule_date)
        TextView txtScheduleDate;
        @BindView(R.id.txt_visited_date)
        TextView txtVisitedDate;
        @BindView(R.id.txt_timeline_visit)
        TextView txtVisitNo;
        @BindView(R.id.recycler_view)
        RecyclerView recyclerView;
        @BindView(R.id.timeline)
        TimelineView timelineView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}