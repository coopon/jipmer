package pondy.coopon.org.pprgjipmer.utils;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtils {

    private static final String TAG = "FileUtils";
    
    public static void unZip(InputStream inputStream, File targetLocation) throws IOException {

        Log.i(TAG, "unZip: called");
        
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
        ZipEntry zipEntry = null;

        while((zipEntry = zipInputStream.getNextEntry()) != null) {
            // if the entry is directory, create it
            // if not write to file.
            if (zipEntry.isDirectory()) {
                File new_dir = new File(targetLocation + "/" + zipEntry);
                new_dir.mkdirs();
            } else {

                Log.d(TAG, "unZip: writing file: " + zipEntry.getName());

                FileOutputStream new_file = new FileOutputStream(targetLocation + "/" + zipEntry.getName());
                byte[] buffer = new byte[1024];
                int length = 0;

                while ((length = zipInputStream.read(buffer)) > 0) {
                    new_file.write(buffer, 0, length);
                }

                zipInputStream.closeEntry();
                new_file.close();
            }
        }

        zipInputStream.close();
    }
}
