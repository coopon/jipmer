package pondy.coopon.org.pprgjipmer.rest;

import com.google.gson.JsonObject;

import org.json.JSONArray;

import java.util.List;

import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.model.User;
import pondy.coopon.org.pprgjipmer.model.Video;
import pondy.coopon.org.pprgjipmer.utils.AppConfig;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface PPRGInterface {

    @POST(AppConfig.API_AUTHENTICATE)
    Call<User> authenticate(@Body JsonObject loginBody);

    @GET(AppConfig.API_GET_MOTHERS)
    Call<List<Mother>> getAllMothers(@Path(value="center_id") int centerId);

    @GET("https://gitlab.com/coopon/pprg/pprg-web/raw/master/resources/data/content/video/pprg_video_playlist.json")
    Call<List<Video>> videoPlaylist();

    @POST(AppConfig.API_POST_MOTHERS_VISITS)
    Call<List<Mother>> updateMothersVisit(@Path(value="center_id") int centerId, @Body List<Mother> modifiedMothers);

}
