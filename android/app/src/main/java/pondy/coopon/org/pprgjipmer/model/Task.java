package pondy.coopon.org.pprgjipmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Task {
    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("answer")
    @Expose
    private Boolean answer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Boolean getAnswer() {
        return answer;
    }

    public void setAnswer(Boolean answer) {
        this.answer = answer;
    }

    public Task(String question, Boolean answer) {
        this.question = question;
        this.answer = answer;
    }

    public Task(Long id, String question, Boolean answer) {
        this.id = id;
        this.question = question;
        this.answer = answer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}