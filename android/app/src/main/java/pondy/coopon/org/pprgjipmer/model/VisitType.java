package pondy.coopon.org.pprgjipmer.model;

import java.util.List;

public class VisitType {

    private String type;
    private List<Task> taskList;
    private String tag;


    public VisitType(String type, List<Task> taskList, String tag) {
        this.type = type;
        this.taskList = taskList;
        this.tag = tag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
