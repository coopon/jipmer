package pondy.coopon.org.pprgjipmer.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.ListenerToken;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryChange;
import com.couchbase.lite.QueryChangeListener;
import com.couchbase.lite.Result;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.dacer.androidcharts.LineView;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.activity.MainActivity;
import pondy.coopon.org.pprgjipmer.helper.DatabaseManager;
import pondy.coopon.org.pprgjipmer.helper.SharedPref;
import pondy.coopon.org.pprgjipmer.model.DashboardData;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.model.Visit;
import pondy.coopon.org.pprgjipmer.utils.DateTimeUtils;
import pondy.coopon.org.pprgjipmer.view.MonthYearPicker;


public class DashboardFragment extends Fragment {

    private static final String TAG = "DashboardFragment";

    // Helper
    private SharedPref sharedPref;
    private DatabaseManager dbMgr;

    // Query listener
    private Query query;
    private ListenerToken queryListenerToken;

    // Views
    @BindView(R.id.text_percentage_chart)
    TextView txtPercentage;
    @BindView(R.id.deco_view_chart)
    DecoView decoView;
    @BindView(R.id.line_view)
    LineView lineView;
    @BindView(R.id.txt_no_of_patient)
    TextView txtNoOfPatients;
    @BindView(R.id.txt_area)
    TextView txtArea;
    @BindView(R.id.txt_visit_month_year)
    TextView txtVisitMonthYear;
    @BindView(R.id.img_date_filter)
    ImageView btnDateFilter;
    @BindView(R.id.card_visits)
    View viewTodayVisit;
    @BindView(R.id.card_records)
    View viewRecords;
    @BindView(R.id.card_areas)
    View viewAreas;

    // Variables
    private DashboardData masterData = new DashboardData();
    private List<Mother> patientList = new ArrayList<>();


    public DashboardFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: called");

        sharedPref = new SharedPref(getContext());
        dbMgr = DatabaseManager.getSharedInstance(getContext());

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, rootView);

        initViewsAndData();

        return rootView;
    }

    private QueryChangeListener queryChangeListener = new QueryChangeListener() {
        @Override
        public void changed(QueryChange change) {

            List<Result> resultList = change.getResults().allResults();

            // No of patients
            masterData.setNoOfPatients(resultList.size());
            if (txtNoOfPatients != null)
                txtNoOfPatients.setText(String.valueOf(masterData.getNoOfPatients()));

            patientList.clear();
            for (Result result : resultList)
                patientList.add(dbMgr.buildPatient(result));

            Calendar c = Calendar.getInstance();
            loadCharts(c.get(Calendar.MONTH), c.get(Calendar.YEAR), true);

        }
    };

    private void initViewsAndData() {

        viewTodayVisit.setOnClickListener(v -> {
            showViewOnViewPager(3);
        });
        viewRecords.setOnClickListener(v -> {
            showViewOnViewPager(1);
        });
        viewAreas.setOnClickListener(v -> {
            showViewOnViewPager(4);
        });


        // Query to get records
        try {
            query = dbMgr.getAllPatients();
            queryListenerToken = query.addChangeListener(queryChangeListener);
            query.execute();

        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }

        // Area view
        if (sharedPref.getCenterName() != null) {
            masterData.setArea(sharedPref.getCenterName());
        }

        txtArea.setText("Center : " + masterData.getArea());

        // Month year Date picker
        MonthYearPicker monthYearPicker = new MonthYearPicker(Objects.requireNonNull(getActivity()));
        monthYearPicker.build(
                (dialog, which) -> loadCharts(monthYearPicker.getSelectedMonthValue(), monthYearPicker.getSelectedYearValue(), false),
                (dialog, which) -> dialog.dismiss());
        monthYearPicker.setTitle(getString(R.string.hint_month_year_dialog_title));

        btnDateFilter.setOnClickListener(view -> monthYearPicker.show());

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (lineView != null) {
                Calendar c = Calendar.getInstance();
                loadCharts(c.get(Calendar.MONTH), c.get(Calendar.YEAR), true);
            }
        }
    }

    private void showViewOnViewPager(int pos){
        ((MainActivity) Objects.requireNonNull(getActivity())).setViewPagerPosition(pos);
    }

    private void loadCharts(int month, int year, boolean isCurrentMonth) {

        List<String> visitCheckList = new ArrayList<>();
        visitCheckList.add(DateTimeUtils.dateNow(DateTimeUtils.dateFormat_yyyy_MM_dd));
        String monthYear = year + "-" + (String.valueOf(month + 1).length() > 1 ? month + 1 : ("0" + (month + 1))) + "-";
        @SuppressLint("UseSparseArrays") HashMap<Integer, Integer> scheduleList = new HashMap<>();
        @SuppressLint("UseSparseArrays") HashMap<Integer, Integer> visitedList = new HashMap<>();
        masterData.setNoOfSchedule(0);
        masterData.setNoOfVisited(0);
        masterData.getScheduleList().clear();
        masterData.getVisitedList().clear();
        for (Mother patient : patientList) {
            if (patient.getVisits() != null && patient.getVisits().size() > 0) {
                int v = 1;
                for (Visit visit : patient.getVisits()) {
                    // Schedule list update in hash map list with date key
                    if (visit.getSchedDate() != null && visit.getSchedDate().contains(monthYear)) {
                        int dateKey = DateTimeUtils.convertStringDateTimeToCalender(visit.getSchedDate(), DateTimeUtils.dateFormat_api).get(Calendar.DATE);
                        scheduleList.put(dateKey, (scheduleList.containsKey(dateKey) && scheduleList.get(dateKey) != null) ? scheduleList.get(dateKey) + 1 : 1);

                        // Calculate No of schedule visits and visited count for current day
                        if (isCurrentMonth && visit.getSchedDate() != null && visitCheckList.contains(visit.getSchedDate())) {
                            masterData.setNoOfSchedule(masterData.getNoOfSchedule() > 0 ? masterData.getNoOfSchedule() + 1 : 1);
                            if (patient.getCurrentVisit() >= v && visit.getVisitedDate() != null && !visit.getVisitedDate().isEmpty())
                                masterData.setNoOfVisited(masterData.getNoOfVisited() > 0 ? masterData.getNoOfVisited() + 1 : 1);
                        }
                    }

                    // Visited list update in hashmap list with date key
                    if (visit.getVisitedDate() != null && !visit.getVisitedDate().isEmpty() && visit.getVisitedDate().contains(monthYear)) {
                        int dateKey = DateTimeUtils.convertStringDateTimeToCalender(visit.getVisitedDate(), DateTimeUtils.dateFormat_api).get(Calendar.DATE);
                        visitedList.put(dateKey, (visitedList.containsKey(dateKey) && visitedList.get(dateKey) != null) ? visitedList.get(dateKey) + 1 : 1);
                    }
                    v++;
                }

            }

        }

        masterData.setScheduleList(scheduleList);
        masterData.setVisitedList(visitedList);

        // Line chart - specific month
        loadLineCharts(month, year);

        // Circle chart - current day info
        if (isCurrentMonth)
            loadCircleVisitChart();
    }

    // Scrollable line chart for visit
    private void loadLineCharts(int month, int year) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        int maxVal = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        ArrayList<String> test = new ArrayList<>();
        ArrayList<Integer> dataScheduleList = new ArrayList<>();
        ArrayList<Integer> dataVisitedList = new ArrayList<>();
        for (int i = 0; i < maxVal; i++) {
            Integer key = i + 1;
            test.add(String.valueOf(key));
            dataScheduleList.add(masterData.getScheduleList().containsKey(key) && masterData.getScheduleList().get(key) != null ? masterData.getScheduleList().get(key) : 0);
            dataVisitedList.add(masterData.getVisitedList().containsKey(key) && masterData.getVisitedList().get(key) != null ? masterData.getVisitedList().get(key) : 0);
        }
        lineView.setBottomTextList(test);
        lineView.setColorArray(new int[]{ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.line_chart_schedule), ContextCompat.getColor(getContext(), R.color.line_chart_visited)
        });
        lineView.setDrawDotLine(true);
        lineView.setShowPopup(LineView.SHOW_POPUPS_NONE);

        ArrayList<ArrayList<Integer>> dataLists = new ArrayList<>();
        dataLists.add(dataScheduleList);
        dataLists.add(dataVisitedList);

        lineView.setDataList(dataLists);

        txtVisitMonthYear.setText(DateTimeUtils.convertCalenderToStringDateTime(calendar, DateTimeUtils.dateFormat_MMM_yyyy));
    }

    // Circle chart for current day visit
    private void loadCircleVisitChart() {

        int mSeries1Index;
        txtPercentage.setText("");

        //Set Circle size out of 360
        decoView.configureAngles(360, 0);

        //Max Size
        final float seriesMax = masterData.getNoOfSchedule() > 0 ? masterData.getNoOfSchedule() : 1;
        final float lineWidth = 10f;
        final int bgColor = ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.gray_100);
        int fillColor = ContextCompat.getColor(getContext(), R.color.fab_color);

        //Default Back
        decoView.addSeries(new SeriesItem.Builder(bgColor)
                .setRange(0, seriesMax, seriesMax)
                .setInitialVisibility(false)
                .setLineWidth(lineWidth)
                .build());

        //Target
        SeriesItem seriesItem1 = new SeriesItem.Builder(fillColor)
                .setRange(0, seriesMax, 0)
                .setInitialVisibility(false)
                .setLineWidth(lineWidth)
                .build();

        mSeries1Index = decoView.addSeries(seriesItem1);
        decoView.executeReset();

        decoView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(0)
                .setDuration(0)
                .build());

        int visitCount = masterData.getNoOfVisited() > 0 ? masterData.getNoOfVisited() : 0;
        decoView.addEvent(new DecoEvent.Builder(visitCount).setIndex(mSeries1Index).setDuration(2000).build());

        txtPercentage.setText("0/0");
        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                // We found a percentage so we insert a percentage
                float percentFilled = (currentPosition - seriesItem1.getMinValue()) / (seriesItem1.getMaxValue() - seriesItem1.getMinValue());
                float val = percentFilled * seriesMax;

                String doneVisit = String.valueOf(val > 0 ? String.format("%.0f", val) : 0);
                String actualVisit = String.valueOf(masterData.getNoOfSchedule() > 0 ? masterData.getNoOfSchedule() : 0);
                String content = "<big>" + doneVisit + "</big><small>/" + actualVisit + "</small>";
                txtPercentage.setText(Html.fromHtml(content));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: called");
        if (queryListenerToken != null && query != null) {
            query.removeChangeListener(queryListenerToken);
            Log.d(TAG, "onDestroy Listener removed");
        }
    }
}