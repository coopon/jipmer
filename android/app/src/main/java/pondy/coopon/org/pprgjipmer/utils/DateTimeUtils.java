package pondy.coopon.org.pprgjipmer.utils;


import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateTimeUtils {

    public static final String dateFormat_YYYY_MM_dd_hh_mm_ss = "yyyy-MM-dd hh:mm:ss";
    public static final String dateFormat_d_MMM_YYYY_hh_mm_ss_a = "d-MMM-yyyy hh:mm:ss a";
    public static final String dateFormat_yyyy_MM_dd = "yyyy-MM-dd";
    public static final String dateFormat_dd_MMM_yyyy = "dd-MMM-yyyy";
    public static final String dateFormat_dd_MM_yyyy = "dd-MM-yyyy";
    public static final String dateFormat_yyyy_MM = "yyyy-MM";
    public static final String dateFormat_MMM_yyyy = "MMM-yyyy";
    public static final String dateFormat_api = "yyyy-MM-dd";


    /**
     * @param date
     *            date in String
     * @param fromFormat
     *            format of your <b>date</b> eg: if your date is 2011-07-07
     *            09:09:09 then your format will be <b>yyyy-MM-dd hh:mm:ss</b>
     * @param toFormat
     *            format to which you want to convert your <b>date</b> eg: if
     *            required format is 31 July 2011 then the toFormat should be
     *            <b>d MMMM yyyy</b>
     * @return formatted date
     */
    public static String convertStringDateFormatToAnother(String date, String fromFormat,
                                     String toFormat) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(fromFormat, Locale.getDefault());
            Date d = simpleDateFormat.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d);

            simpleDateFormat = new SimpleDateFormat(toFormat, Locale.getDefault());
            simpleDateFormat.setCalendar(calendar);
            date = simpleDateFormat.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String convertCalenderToStringDateTime(Calendar calendar, String toFormat) {
        String date = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(toFormat, Locale.getDefault());
            simpleDateFormat.setCalendar(calendar);
            date = simpleDateFormat.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public static Calendar convertStringDateTimeToCalender(String dateTime, String currentFormat)
    {
        Calendar calendar = null;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(currentFormat, Locale.getDefault());
            Date d = simpleDateFormat.parse(dateTime);
            calendar = Calendar.getInstance();
            calendar.setTime(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }

    public static String addDaysToAlter(Calendar calendar, int noOfDays) {
        Calendar c = Calendar.getInstance();
        c.setTime(calendar.getTime());
        c.add(Calendar.DAY_OF_YEAR, noOfDays);
        return convertCalenderToStringDateTime(c, dateFormat_yyyy_MM_dd);
    }

    public static String convertDateToString(Date date, String toFormat)
    {
        SimpleDateFormat dest = new SimpleDateFormat(toFormat, Locale.getDefault());
        return dest.format(date);
    }

    public static String dateNow(String toFormat)
    {
        return new SimpleDateFormat(toFormat, Locale.getDefault()).format(new Date());
    }

    public static int differentBetween2Days(Calendar calendar1, Calendar calendar2) {
        //Comparing dates
        long difference = calendar1.getTime().getTime() - calendar2.getTime().getTime();
        long differenceDates = difference / (24 * 60 * 60 * 1000);
        return (int) differenceDates;
    }

    /**
     * Add no of working days with particular date
     * @param baseDate
     *          Start date
     * @param noOfWorkingDays
     *          No of working days want to add
     * @return
     *          Calender object with adding no of working working days
     */
    public static Calendar addWorkingDaysToAlter(Calendar baseDate, int noOfWorkingDays) {
        Calendar result = Calendar.getInstance();
        result.setTime(baseDate.getTime());
        if (noOfWorkingDays < 1) {
            return result;
        }

        result.add(Calendar.DAY_OF_YEAR, noOfWorkingDays);

        /*
         * Check result is working day or not
         * If working date no change
         * Other wise move to next working date
         */
        if(result.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
            result.add(Calendar.DAY_OF_WEEK, 2); // Next working day comes after 2 days
        else if(result.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
            result.add(Calendar.DAY_OF_WEEK, 1); // Next working day comes after 1 days

        return result;
    }

    /**
     * Checking particular  date is working day or not
     * @param cal
     *          date want to check
     * @return boolean
     *          if it is working day return true
     *          other wise false
     */
    private static boolean isWorkingDay(Calendar cal) {
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        return dayOfWeek != Calendar.SUNDAY && dayOfWeek != Calendar.SATURDAY;
    }
}
