package pondy.coopon.org.pprgjipmer.model;

import java.util.List;

public class TimeLineModel {
        private String scheduleDate, visitedDate;
        private Integer visitNo;
        private List<Task> visitQuestions;
        private int status;

        public TimeLineModel(int visitNo, String scheduleDate, String visitedDate, List<Task> visitQuestions, int status) {
            this.visitNo = visitNo;
            this.scheduleDate = scheduleDate;
            this.visitedDate = visitedDate;
            this.visitQuestions = visitQuestions;
            this.status = status;
        }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getVisitedDate() {
        return visitedDate;
    }

    public void setVisitedDate(String visitedDate) {
        this.visitedDate = visitedDate;
    }

    public Integer getVisitNo() {
        return visitNo;
    }

    public void setVisitNo(Integer visitNo) {
        this.visitNo = visitNo;
    }

    public List<Task> getVisitQuestions() {
        return visitQuestions;
    }

    public void setVisitQuestions(List<Task> visitQuestions) {
        this.visitQuestions = visitQuestions;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}