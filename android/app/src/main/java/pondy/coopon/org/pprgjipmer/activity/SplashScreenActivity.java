package pondy.coopon.org.pprgjipmer.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import pondy.coopon.org.pprgjipmer.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        /*
         not using a layout file to display splash screen.
         instead check res/drawable/splash_background.xml
         */

        /*
         following handler will close this splash screen
         after delayMillis and starts the welcome screen.
         */
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }, 1000);
    }
}