package pondy.coopon.org.pprgjipmer.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.helper.SharedPref;


public class MyProfileFragment extends Fragment {

    private static final String TAG = "MyProfileFragment";

    // Helper
    private SharedPref sharedPref;

    @BindView(R.id.txt_worker_name)
    TextView txtWorkerName;
    @BindView(R.id.txt_worker_about)
    TextView txtWorkerAbout;


    public MyProfileFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        sharedPref = new SharedPref(getContext());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, rootView);

        String name = "Name : " + sharedPref.getName();
        txtWorkerName.setText(name);
        String about = "Center : " + sharedPref.getCenterName();
        txtWorkerAbout.setText(about);

        return rootView;
    }
}