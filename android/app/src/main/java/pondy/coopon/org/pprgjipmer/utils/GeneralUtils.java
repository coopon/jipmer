package pondy.coopon.org.pprgjipmer.utils;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.github.clans.fab.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import coopon.manimaran.about_us_activity.AboutActivityBuilder;
import pondy.coopon.org.pprgjipmer.BuildConfig;
import pondy.coopon.org.pprgjipmer.R;


public class GeneralUtils {

    public static boolean isNetworkConnected(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = cm.getActiveNetworkInfo();
        return nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
    }

    public static void fabHideOnScroll(RecyclerView recyclerView, final FloatingActionButton fab) {

        //When scroll to hide fab
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide(true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fab.show(true);
                        }
                    }, 2000);
                } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                    fab.show(true);
                }
            }
        });
    }

    public static String getRootDirPath(Context context) {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File file = ContextCompat.getExternalFilesDirs(context.getApplicationContext(),
                    null)[0];
            return file.getAbsolutePath();
        } else {
            return context.getApplicationContext().getFilesDir().getAbsolutePath();
        }
    }

    public static String getProgressDisplayLine(long currentBytes, long totalBytes) {
        return getBytesToMBString(currentBytes) + "/" + getBytesToMBString(totalBytes);
    }

    private static String getBytesToMBString(long bytes){
        return String.format(Locale.ENGLISH, "%.2fMb", bytes / (1024.00 * 1024.00));
    }

    public static Location convertStringToLocation(String location){
        if(location != null && location.contains(",")){
            Location loc = new Location("dummyprovider");
            loc.setLatitude(Double.parseDouble(location.split(",")[0]));
            loc.setLongitude(Double.parseDouble(location.split(",")[1]));
            return loc;
        }
        return null;
    }

    public static void aboutUsActivity(Context context) {

        // Base
        int appTheme = R.style.AppTheme;
        String aboutUsActivityTitle = "About Us";

        // App Details
        int appLogo = R.drawable.ic_mascot_circle;
        String appName = context.getString(R.string.app_name);
        String appAbout = "Post Pregnancy Health Care and how it might change the nutrition level of both Mother and the Child after effectively delivering demonstrable and engaging contents through JIPMER's health care network of Field Health Offers and Workers who would closely work with the Consented Pregnant Mothers in a scheduled manner.";
        String appVersion = "Version : " + BuildConfig.VERSION_NAME;
        String appPlayStoreLink = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;


        // License details
        String licenseName = "GPL v3";
        String licenseUrl = "https://www.gnu.org/licenses/gpl-3.0.txt";

        // Share Details
        String shareMsgText = appName + "\n" + appAbout + "\nLink : " + appPlayStoreLink + "\nShare with your friends";
        String shareIntentTitle = "Choose app to share...";

        // Powered By
        int poweredByIcon = R.drawable.coopon;
        String poweredByTitle = "Powered By";
        String poweredByName = "Coopon";
        String poweredByAbout = "Coopon is a (science & technology) worker's cooperative seeking to find economic sustainability through the use of Free S/W, H/W, & Design tools, skills, business models in the context of commons based peer production. ";
        String poweredByLink = "http://cooponscitech.in/";

        // Initiated By
        int initiatedByIcon = 0;//R.drawable.ic_initiator;
        String initiatedByTitle = "Initiated By";
        String initiatedByName = "Dr. Bala & Dr. Jayalakshmi";
        String initiatedByAbout = "Dr. Bala - Student of JIPMER, \nGuided By :\nDr. Jayalakshmi - JIPMER Social and Preventive Health Care Medicine department";
        String initiatedByLink = "balachandiran2012@gmail.com";

        // Others
        String sourceCodeLink = "https://gitlab.com/coopon/jipmer/tree/master/android";
        int jsonResOfThirdPartyLibrary = R.raw.third_party_library;
        int jsonResOfCredits = R.raw.credits;
        String contactMail = "support@cooponscitech.in";


        /*
         *  All the values are set in about activity builder
         *  If we don't need any views just ignore in build.
         */
        new AboutActivityBuilder.Builder(context)
                .setTitle(aboutUsActivityTitle)
                .setAppLogo(appLogo)
                .setAppName(appName)
                .setAppAbout(appAbout)
                .setAppVersion(appVersion)
                .setLicense(licenseName, licenseUrl)
                .setShare(shareMsgText, shareIntentTitle)
                .setRateUs(appPlayStoreLink)
                .setInitiatedBy(poweredByIcon, poweredByTitle, poweredByName, poweredByAbout, poweredByLink)
                .setPoweredBy(initiatedByIcon, initiatedByTitle, initiatedByName, initiatedByAbout, initiatedByLink)
                .setSeeSourceCode(sourceCodeLink)
                .setThirdPartyLibrary(jsonResOfThirdPartyLibrary)
                .setCredits(jsonResOfCredits)
                .setContactUs(contactMail)
                .showAboutActivity();



    }

    public static String getAssetVideoJsonData(Context context) {
        String json;
        try {
            InputStream is = context.getAssets().open("videos/pprg_video_playlist.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("VIDEO PLAY LIST", json);
        return json;
    }
}
