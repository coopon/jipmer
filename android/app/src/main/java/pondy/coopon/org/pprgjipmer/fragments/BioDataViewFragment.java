package pondy.coopon.org.pprgjipmer.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.github.clans.fab.FloatingActionButton;

import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.activity.AddEditBioDataActivity;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.utils.DateTimeUtils;
import pondy.coopon.org.pprgjipmer.utils.GeneralUtils;


public class BioDataViewFragment extends Fragment {

    private static final String TAG = "BioDataViewFragment";


    @BindView(R.id.edit_name)
    EditText editName;
    @BindView(R.id.edit_age)
    EditText editAge;
    @BindView(R.id.edit_phone)
    EditText editPhone;
    @BindView(R.id.edit_addr_dr_no)
    EditText editDrNo;
    @BindView(R.id.edit_addr_street_name)
    EditText editStreetName;
    @BindView(R.id.edit_addr_area)
    EditText editArea;
    @BindView(R.id.edit_addr_city)
    EditText editCity;
    @BindView(R.id.edit_addr_pincode)
    EditText editPinCode;
    @BindView(R.id.edit_location)
    EditText editLocation;
    @BindView(R.id.edit_patient_id)
    EditText editPatientId;
    @BindView(R.id.edit_delivery_date)
    EditText editDeliveryDate;
    @BindView(R.id.edit_child_weight)
    EditText editChildWeight;
    @BindView(R.id.edit_child_gender)
    EditText editChildGender;
    @BindView(R.id.edit_blood_group)
    EditText editBloodGroup;
    @BindView(R.id.layout_child_information)
    LinearLayout layoutChildInformation;
    @BindView(R.id.smooth_progress_bar)
    SmoothProgressBar progressBar;
    @BindView(R.id.fab_edit)
    FloatingActionButton fabEdit;


    private Mother masterPatient;

    public BioDataViewFragment(Mother patient) {
        this.masterPatient = patient;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bio_data_view, container, false);
        ButterKnife.bind(this, view);


        if (masterPatient != null) {
            editName.setText(masterPatient.getName() != null ? masterPatient.getName() : "");
            editAge.setText(String.valueOf(masterPatient.getAge() != null ? masterPatient.getAge() : 0));
            editPhone.setText(masterPatient.getPhone() != null ? masterPatient.getPhone() : "");
            editDrNo.setText(masterPatient.getDrNo() != null ? masterPatient.getDrNo() : "");
            editStreetName.setText(masterPatient.getStreetName() != null ? masterPatient.getStreetName() : "");
            editArea.setText(masterPatient.getArea() != null ? masterPatient.getArea() : "");
            editCity.setText(masterPatient.getCity() != null ? masterPatient.getCity() : "");
            editPinCode.setText(masterPatient.getPinCode() != null ? masterPatient.getPinCode() : "");
            editPatientId.setText(masterPatient.getMotherId() != null ? masterPatient.getMotherId().toString() : "");

            if (masterPatient.getDeliveryDate() != null) {
                Calendar dateOfDelivery = DateTimeUtils.convertStringDateTimeToCalender(masterPatient.getDeliveryDate(), DateTimeUtils.dateFormat_api);
                editDeliveryDate.setText(DateTimeUtils.convertCalenderToStringDateTime(dateOfDelivery, DateTimeUtils.dateFormat_dd_MMM_yyyy));

                editChildGender.setText(masterPatient.getChildGender().equals("male") ? "Male" : "Female");
                editChildWeight.setText(String.valueOf(masterPatient.getChildWeight()));

                layoutChildInformation.setVisibility(View.VISIBLE);
            } else
                editDeliveryDate.setVisibility(View.GONE);

            if (masterPatient.getLocation() != null) {
                Location location = GeneralUtils.convertStringToLocation(masterPatient.getLocation());
                if(location != null) {
                    String loc = "lat : " + String.format(Locale.ENGLISH, "%.4f", location.getLatitude()) + ", "
                            + "lon : " + String.format(Locale.ENGLISH, "%.4f", location.getLongitude());
                    editLocation.setText(loc);
                }else
                    editLocation.setText("");
            }

            editBloodGroup.setText(masterPatient.getBloodGroup());
        }

        fabEdit.setOnClickListener(v -> {
            Intent outgoing = new Intent(getContext(), AddEditBioDataActivity.class);
            outgoing.putExtra("PATIENT_ID", masterPatient.getMainId());
            startActivity(outgoing);
        });
        fabEdit.setVisibility(View.GONE);
        editName.setEnabled(false);
        editAge.setEnabled(false);
        editPhone.setEnabled(false);
        editPatientId.setEnabled(false);
        editArea.setEnabled(false);
        editCity.setEnabled(false);
        editLocation.setEnabled(false);
        editPinCode.setEnabled(false);
        editDrNo.setEnabled(false);
        editStreetName.setEnabled(false);
        editDeliveryDate.setEnabled(false);
        editChildWeight.setEnabled(false);
        editBloodGroup.setEnabled(false);
        editChildGender.setEnabled(false);

        int textColor = Color.BLACK;
        editName.setTextColor(textColor);
        editAge.setTextColor(textColor);
        editArea.setTextColor(textColor);
        editPhone.setTextColor(textColor);
        editPinCode.setTextColor(textColor);
        editPatientId.setTextColor(textColor);
        editCity.setTextColor(textColor);
        editChildWeight.setTextColor(textColor);
        editDeliveryDate.setTextColor(textColor);
        editStreetName.setTextColor(textColor);
        editLocation.setTextColor(textColor);
        editDrNo.setTextColor(textColor);
        editChildGender.setTextColor(textColor);
        editBloodGroup.setTextColor(textColor);

        return view;
    }

}
