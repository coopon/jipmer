package pondy.coopon.org.pprgjipmer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import pondy.coopon.org.pprgjipmer.utils.DateTimeUtils;

public class Visit {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("completed")
    @Expose
    private Boolean completed;
    @SerializedName("sched_date")
    @Expose
    private String schedDate;
    @SerializedName("checklist")
    @Expose
    private CheckList checklist;
    @SerializedName("visited_date")
    @Expose
    private String visitedDate;
    @SerializedName("geolocation")
    @Expose
    private String visitedLocation;

    public String getSchedDate() {
        return schedDate;
    }

    public void setSchedDate(String schedDate) {
        this.schedDate = schedDate;
    }

    public CheckList getChecklist() {
        return checklist;
    }

    public void setChecklist(CheckList checklist) {
        this.checklist = checklist;
    }

    public String getVisitedDate() {
        return visitedDate;
    }

    public void setVisitedDate(String visitedDate) {
        this.visitedDate = visitedDate;
    }

    public String getVisitedLocation() {
        return visitedLocation;
    }

    public void setVisitedLocation(String visitedLocation) {
        this.visitedLocation = visitedLocation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "Visit{" +
                "schedDate='" + schedDate + '\'' +
                ", checklist=" + checklist +
                ", visitedDate='" + visitedDate + '\'' +
                ", visitedLocation='" + visitedLocation + '\'' +
                '}';
    }
}
