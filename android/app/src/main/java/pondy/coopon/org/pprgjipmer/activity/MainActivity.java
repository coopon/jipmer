package pondy.coopon.org.pprgjipmer.activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Query;
import com.couchbase.lite.Result;
import com.couchbase.lite.ResultSet;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import pondy.coopon.org.pprgjipmer.R;
import pondy.coopon.org.pprgjipmer.fragments.DashboardFragment;
import pondy.coopon.org.pprgjipmer.fragments.MapViewFragment;
import pondy.coopon.org.pprgjipmer.fragments.MyProfileFragment;
import pondy.coopon.org.pprgjipmer.fragments.PatientRecordFragment;
import pondy.coopon.org.pprgjipmer.fragments.ScheduleFragment;
import pondy.coopon.org.pprgjipmer.helper.DatabaseManager;
import pondy.coopon.org.pprgjipmer.helper.SharedPref;
import pondy.coopon.org.pprgjipmer.interfaces.MapInterface;
import pondy.coopon.org.pprgjipmer.model.Mother;
import pondy.coopon.org.pprgjipmer.recevier.AlarmNotificationReceiver;
import pondy.coopon.org.pprgjipmer.utils.DateTimeUtils;
import pondy.coopon.org.pprgjipmer.utils.GeneralUtils;
import pondy.coopon.org.pprgjipmer.utils.ToastUtils;
import pondy.coopon.org.pprgjipmer.view.CustomViewPager;

public class MainActivity extends AppCompatActivity implements MapInterface {

    private static final String TAG = "MainActivity";

    private DatabaseManager dbMgr;
    private SharedPref pref;

    @BindView(R.id.main_tabs)
    TabLayout tabLayout;
    @BindView(R.id.container)
    CustomViewPager mViewPager;

    private String[] titleList;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Butter knife Init
        ButterKnife.bind(this);

        dbMgr = DatabaseManager.getSharedInstance(getApplicationContext());
        pref = new SharedPref(getApplicationContext());

        // Check user login or not
        pref.checkLogin(this);

        // Title List of Action bar
        titleList = new String[]{
                this.getResources().getString(R.string.main_tab_text_1),
                this.getResources().getString(R.string.main_tab_text_2),
                this.getResources().getString(R.string.main_tab_text_3),
                this.getResources().getString(R.string.main_tab_text_4),
                this.getResources().getString(R.string.main_tab_text_5)
        };

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        FragmentPagerAdapter mViewPageAdapter = new ViewPageAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mViewPageAdapter);
        mViewPager.setSwipeDisable(true);
        mViewPager.setOffscreenPageLimit(5);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setTabValues(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        // Open tab based on intent otherwise open default one
        if (Objects.equals(getIntent().getAction(), "OPEN_SCHEDULE_FRAGMENT"))
            mViewPager.setCurrentItem(3);
        else
            setTabValues(tabLayout.getSelectedTabPosition());

        // Init Notification service - call on new login
        if (pref.isNewLogin())
            initNotificationService();

    }

    private void setTabValues(int selectedTabPosition) {
        setTitle(titleList[selectedTabPosition]);
        final int normalTab = ContextCompat.getColor(getApplicationContext(), R.color.tab_default);
        final int selectedTab = ContextCompat.getColor(getApplicationContext(), R.color.tab_selected);
        for (int i = 0; i < tabLayout.getTabCount(); i++)
            Objects.requireNonNull(Objects.requireNonNull(tabLayout.getTabAt(i)).getIcon()).setColorFilter(selectedTabPosition == i ? selectedTab : normalTab, PorterDuff.Mode.SRC_IN);
    }

    public void setViewPagerPosition(int pos){
        if(mViewPager != null)
            mViewPager.setCurrentItem(pos);
    }

    public class ViewPageAdapter extends FragmentPagerAdapter {
        ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {
                case 0:
                    fragment = new DashboardFragment();
                    break;
                case 1:
                    fragment = new PatientRecordFragment();
                    break;
                case 2:
                    fragment = new MapViewFragment();
                    break;
                case 3:
                    fragment = new ScheduleFragment();
                    break;
                case 4:
                    fragment = new MyProfileFragment();
                    break;
                default:
                    fragment = new DashboardFragment();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return tabLayout.getTabCount();
        }
    }

    /*
     * android auto assigns tags to fragments on creation through
     * viewPagers. given view pager ID and fragment's position,
     * this function will return the tag as String.
     */
    private String makeFragmentTag(int viewPagerID, int position) {
        return "android:switcher:" + viewPagerID + ":" + position;
    }

    @Override
    public void patientLocation(Location location, String patientID, String patientName) {

        // bring MapViewFragment to user's focus
        mViewPager.setCurrentItem(2);

        FragmentManager manager = getSupportFragmentManager();
        MapViewFragment mapViewFragment = (MapViewFragment) manager.findFragmentByTag(
                makeFragmentTag(mViewPager.getId(), 2)
        );

        if (mapViewFragment != null)
            mapViewFragment.addPatientToMap(location, patientID, patientName);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce)
            super.onBackPressed();
        else {
            this.doubleBackToExitPressedOnce = true;
            ToastUtils.showWarning(getApplicationContext(), getString(R.string.msg_to_exit));
        }
        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_my_profile:
                mViewPager.setCurrentItem(tabLayout.getTabCount() - 1);
                return true;
            case R.id.menu_notification:
                showNotification();
                return true;
            case R.id.menu_logout:
                logout();
                return true;
            case R.id.menu_about_us:
                GeneralUtils.aboutUsActivity(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }



    /**
     * Logout method
     */
    private void logout() {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(getString(R.string.hint_confirmation))
                .setMessage(getString(R.string.msg_do_you_want_logout))
                .setPositiveButton(android.R.string.yes,
                        (dialog, whichButton) -> {
                            SharedPref sharedPref = new SharedPref(getApplicationContext());
                            sharedPref.logoutUser(); // Clear session details*/
                        })
                .setNegativeButton(android.R.string.no,
                        null)
                .show();
    }


    /**
     * Init notification
     */
    private void initNotificationService() {

        Log.d("ALARM - CALL ", "Notification Main activity init");

        /* Retrieve a PendingIntent that will perform a broadcast */
        Intent alarmIntent = new Intent(getApplicationContext(), AlarmNotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 100, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        /* Set the alarm to start at 9:00 AM */
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 9);
        calendar.set(Calendar.MINUTE, 0);
        manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }


    /**
     * Show Notification
     */
    private void showNotification() {

        List<Mother> list = new ArrayList<>();

        // Get today visits information
        try {
            Query query = dbMgr.getScheduleRecordsOnParticularDate(DateTimeUtils.dateNow(DateTimeUtils.dateFormat_yyyy_MM_dd));
            ResultSet resultSet = query.execute();
            for (Result result : resultSet)
                list.add(dbMgr.buildPatient(result));
        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }


        if (list.size() > 0) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            String channelId = getString(R.string.notify_channel_id);
            String notifyTitle = getString(R.string.notify_title);

            /* Add Big View Specific Configuration */
            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

            // Sets a title for the Inbox style big view
            inboxStyle.setBigContentTitle(getString(R.string.notify_big_content_title));

            // Moves events into the big view
            int i = 1;
            for (Mother patient : list) {
                inboxStyle.addLine(i++ + ". " + patient.getName() + ", " + patient.getArea());
            }

            String contentText = "You have " + list.size() + (list.size() > 1 ? " visits" : " visit") + " today.";

            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                CharSequence channelName = getString(R.string.notify_channel_name);
                String description = getString(R.string.notify_channel_description);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, importance);
                notificationChannel.setDescription(description);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300});
                notificationManager.createNotificationChannel(notificationChannel);

                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                notificationManager.createNotificationChannel(notificationChannel);
            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.drawable.ic_mascot)
                    .setContentTitle(notifyTitle)
                    .setContentText(contentText)
                    .setStyle(inboxStyle)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            Intent notificationIntent = new Intent(this, MainActivity.class);
            notificationIntent.setAction("OPEN_SCHEDULE_FRAGMENT");
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, Intent.FILL_IN_ACTION);
            builder.setContentIntent(contentIntent);

            // notificationId is a unique int for each notification that you must define
            notificationManager.notify(201, builder.build());
        } else // No notification have then inform to user
            ToastUtils.showWarning(getApplicationContext(), getString(R.string.notify_no_notification));

    }

    /**
     * Intent call by notification
     *
     * @param intent
     */
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (Objects.equals(intent.getAction(), "OPEN_SCHEDULE_FRAGMENT"))
            if (mViewPager != null)
                mViewPager.setCurrentItem(3);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: called");
    }
}
