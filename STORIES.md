# User Stories

Sequential listing of how each stakeholder interacts with the parts of the system.

## Stakeholder List 

* Health Worker 
* Medical Officer 
* Mother
* Researcher

## Health Woker 

* Opens app 
* Receive current information on mothers needing visit
* Visit mother 
* Access health education content
* Update meeting details with location, time & notes
* Review future meetings, rescheduling 
* Repeat

## Medical Officer / Researcher 

* See overview at the PHC level
* Access / update information on mothers needing visits
* Manual alerts to health workers & mothers 
* View current information on health workers' status 
  * Meetings attended 
  * Meetings missed 
  * Meetings ready 
* Access meeting level information
  * GPS points 
  * Accessed video content?
* Export necessary data in a CSV format

## Mother 

* See upcoming meetings
* View health education content 
* Give feedback on meetings
