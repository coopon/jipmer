% Quotation of costs to develop & maintain Android application and  backend system  for research by Preventive & Social Medicine, JIPMER 
% 20 June 2019 

# Service Provider 

> For COOPON SCITECH 

> Kamalavelan, C-16 Chanakya Apartments, Sithankudi, Puducherry - 605 013
  | 9791876634 | kamal@cooponscitech.in

> Prasanna Venkadesh, 14, B-1, Raj Apartments, 2^nd^ Main Road, Aarudra
  Nagar, Goundanpalayam, Pududcherry - 605 009 | 9940325718 | prasanna@cooponscitech.in

# Project Background

Dr. Jayalakshmy R (Associate Professor, P&SM, JIPMER) &
Dr. Balachandiran are engaging in research to ascertain the impact of
electronic visual aids in imparting health education to new mothers post
childbirth. They will be working with Govt. of Pondicherry Health Department's health workers to
conduct this research. This requires necessary technical support to create and maintain the software infrastructure to support the research.

# Scope of services to be rendered

To develop and maintain android app and required backend system to facilitate the
research. This should be done in a way that invites repetitive usage by
the stakeholders while collecting accurate data for the research. The
services to be rendered require a development time of four weeks folllowed by a maintenance period of one year.

# Stakeholder needs

## Researcher 

1. Needs to know various metrics to ascertain the course of research.
2. Needs to export research data.
3. Needs to be certain that health worker visited the mother.
4. Needs to be certain that visual content was accessed during the meeting.

## Health Worker

1. Needs to use visual aids in the form of images and video clips to educate new mothers.
2. Needs to know current information on new mothers, their contact & location information.

## Mother

1. Needs to be able to review the information provided by health worker.
2. Needs to know updates / future visits by the health worker.

# Assumptions

The assumptions based on which this quotation has been prepared are:-

1. There would be not more than 200 concurrent users. 
  - Assuming the app is deployed to not more than 20 health workers
  working with 5-7 mothers each.
2. Most users will have an Android tablet or smartphone with atleast
Android version 4.4 (Lolipop) or higher. 
  - As per Google's statistics on
10/7/2019, more than 80% of Android devices today are running Android 4.4 or higher.
 # Deliverables 

The development will happen in a weekly iterative fashion with app
delivery in two stages.

| Deliverable | Time | % in total completion | 
| --- | --- | --- | 
| Stage 1 | 2 weeks | 65 % |
| Stage 2 | 2 weeks | 100 % |

## Stage 1 

This will be delivered in 2 weeks time and it will consist of an android app that can :- 

1. Play offline & online videos.
2. Mark current GPS location.
3. Simple user interface.
4. Work with static mock data.

The Android application may not facilitate data entry and retrieval at this point in
time.

## Stage 2 

This will be delivered in 2 weeks time after Stage 1 is delivered. The
Android application at this stage will feature the following :-

1. Polished user experience and interaction.
2. Facilitate data entry and retrieval. 
3. Show PHC & mother's locations on an offline map.
4. Accomodate an overall dashboard view for appointments with the
mothers.
1. Web frontend to create users, view overall statistics at the center level and 

# Cost Estimation

## Software Development 

| Tasks               | Cost for 4 weeks |
| ---                 | ---              |
| Backend             | Rs. 10,250       |
| Web development     | Rs. 10,250       |
| Android development | Rs. 37,550       |
| UI/UX Design        | Rs. 15,750       |
| Total               | Rs. 73,800       |

## Annual Maintenance 

Maintenance includes the following services:

1. On-call support from 10:00AM to 18:00 to JIPMER representatives
*only* on all business days (Mon - Fri) for the duration of the maintenance period.
2. Fixing bugs, if any, in the application / system.
3. Maintaining server's performance and security of data.
4. Backup of data.

| Expenditure           | Cost  |
| ---                   | ---   |
| Developer Salary      | 6,500 |
| System Administration | 5,000 |
| On-call support       | 3,000 |

| Period   | Total Cost |
| ---      | ---        |
| 52 weeks | Rs. 14,500 |

## Server & Bandwidth cost 

1. A virtual private server will be rented and operated through a cloud service provider at Bengaluru. 
2. The server will have the following system specifications.

| Component | Specification |
| ---       | ---           |
| CPU       | Intel Xeon    |
| RAM       | 2 GB          |
| OS        | Ubuntu 18.04  |
| Storage   | 50 GB         |
| Bandwidth | 2 TB          |

3. Monthly server cost calculation

| Particulars | Monthly Cost |
| ---         | ---          |
| Server      | Rs. 738      |
| GST @ 18%   | Rs. 162      |
| Total       | Rs. 900      |

4. Total cost for a year comes to Rs. 10,800. 

| Particulars | Time     | Total Cost |
| ---         | ---      | ---        |
| Server      | 52 weeks | Rs. 10,800 |

# Cost Summary

This is the quotation for costs incurred for development, maintenance and server costs.

| Particulars | Time     | Cost       |
| ---         | ---      | ---        |
| Development | 4 weeks  | Rs. 73,800 |
| Maintenance | 52 weeks | Rs. 13,500 |
| Server      | 52 weeks | Rs. 10,800 |


| Total Cost   |
| ---          |
| Rs. 98,100   |

* GST not applicable
 

# Payment Schedule 

| Particulars                         | Time                                    | Amount     |
| ---                                 | ---                                     | ---        |
| Deliverable 1                       | Est. 2 week since start of the project  | Rs. 36,900 |
| Deliverable 2, Maintenance & Server | Est. 4 weeks since start of the project | Rs. 61,200 |

## Account Details

| Particulars | Details                          |
| ---         | ---                              |
| Name        | Kamalavelan S                    |
| A/c No.     | 38221272053                      |
| Bank        | State Bank of India              |
| Branch      | Easwaran Koil Street, Puducherry |
| IFS Code    | SBIN0070601                      |
| PAN No.     | DOOPK1512J                       |

# Copyrights

Appropriate industry standard licenses have been chosen to ensure
that JIPMER can continue to use / modify / redistribute the source code
in perpetuity.

Copyrights for all code is licensed under GNU General Public License version 3.

Copyrights for all artwork is under Creative Commons Attribution Share-Alike 4.0

